#ifndef HELPERS_H
#define HELPERS_H

byte getByteValue(String msg, int dataValues, char splitChar){
  byte value0 = 0;
  byte value1 = 0;
  byte ret;
  if(dataValues > 1){
    for(byte indexValue = 0; indexValue < dataValues - 1; indexValue++){
      value0 = msg.indexOf(splitChar, value0 + 1);
    }
    value1 = msg.indexOf(splitChar, value0 + 1);
    msg.remove(value1);
    msg.remove(0, value0 + 1);
  }else{
    value0 = msg.indexOf(splitChar, value0 + 1);
    msg.remove(value0);
  }
  
  ret = msg.toInt();
  //Serial.print("Free heap: "); Serial.println(esp_get_free_heap_size());
  return ret;
}

int getIntValue(String msg, int dataValues, char splitChar){
  byte value0 = 0;
  byte value1 = 0;
  int ret;
  if(dataValues > 1){
    for(byte indexValue = 0; indexValue < dataValues - 1; indexValue++){
      value0 = msg.indexOf(splitChar, value0 + 1);
    }
    value1 = msg.indexOf(splitChar, value0 + 1);
    msg.remove(value1);
    msg.remove(0, value0 + 1);
  }else{
    value0 = msg.indexOf(splitChar, value0 + 1);
    msg.remove(value0);
  }
  ret = msg.toInt();
  //Serial.print("Free heap: "); Serial.println(esp_get_free_heap_size());
  return ret;
}

bool getBoolValue(String msg, int dataValues, char splitChar){
  byte value0 = 0;
  byte value1 = 0;
  if(dataValues > 1){
    for(byte indexValue = 0; indexValue < dataValues - 1; indexValue++){
      value0 = msg.indexOf(splitChar, value0 + 1);
    }
    value1 = msg.indexOf(splitChar, value0 + 1);
    msg.remove(value1);
    msg.remove(0, value0 + 1);
    //Serial.print("Free heap: "); Serial.println(esp_get_free_heap_size());
    if(msg == "0"){return false;}else{return true;}
  }else{
    value0 = msg.indexOf(splitChar, value0 + 1);
    msg.remove(value0);
    //Serial.print("Free heap: "); Serial.println(esp_get_free_heap_size());
    if(msg == "0"){return false;}else{return true;}
  }
}

String getStringValue(String msg, int dataValues, char splitChar){
  if(dataValues != 0){
    msg.replace(" ", "");
    byte value0 = 0;
    byte value1 = 0;

    if(dataValues > 1){
      for(byte indexValue = 0; indexValue < dataValues - 1; indexValue++){
        value0 = msg.indexOf(splitChar, value0 + 1);
      }
      value1 = msg.indexOf(splitChar, value0 + 1);
      msg.remove(value1);
      msg.remove(0, value0 + 1);
    }else{
      value0 = msg.indexOf(splitChar, value0 + 1);
      msg.remove(value0);
    }
  }else{
    byte value0 = 0;
    value0 = msg.indexOf(splitChar, value0);
    msg.remove(value0);
  }
  //Serial.print("Free heap: "); Serial.println(esp_get_free_heap_size());
  return msg;
}

#endif
