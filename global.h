#ifndef GLOBAL_H
#define GLOBAL_H

typedef struct strDNI {
	int			  tramite;
	String		apellido;
	String		nombre;
	String		sexo;
	int		    dni;
	String		ejemplar;
	String		nacimiento;
	String		emision;
} dni;

typedef struct strWgCom {
	byte      pulse;
	int       interval;
} wgComm;

typedef struct strWgFormat {
	byte      size;
  byte      oddBit;
  unsigned long      oddMaskLow;
  unsigned long      oddMaskHigh;
  byte      evenBit;
  unsigned long      evenMaskLow;
  unsigned long      evenMaskHigh;
} wgFormat;

typedef struct strWiFi {
  bool      hidden = false;
  String    apSSID = "JPH-DNI";
  String    apPass = "JphlionsDev";
  String    SSID = "JPH-Dev";
  String    Pass = "JphlionsDev";
  bool      ap = true;
  bool      client = true;
  bool      enabled = true;
} wifiConf;

typedef struct strSerialConf {
  int       baudRate = 115200;
} serialConf;

#endif
