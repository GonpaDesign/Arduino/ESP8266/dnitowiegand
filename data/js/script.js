var tab_pane;

// Button events
$('#wg_save').click(function(){
  setWgCom();
  setWgFormat();
});
$('#serial_modify').click(function(){
  setSerial();
});
$('#wifi_sta').click(function(){
  if($('#wifi_sta').html() == 'Off')
  {
      $('#wifi_sta').html("On");
      $('#wifi_sta').removeClass('btn-danger');
      $('#wifi_sta').addClass('btn-success');
    }else{
      $('#wifi_sta').html("Off");
      $('#wifi_sta').removeClass('btn-success');
      $('#wifi_sta').addClass('btn-danger');
    }
});
$('#wifi_ap').click(function(){
  if($('#wifi_ap').html() == 'Off')
  {
      $('#wifi_ap').html("On");
      $('#wifi_ap').removeClass('btn-danger');
      $('#wifi_ap').addClass('btn-success');
    }else{
      $('#wifi_ap').html("Off");
      $('#wifi_ap').removeClass('btn-success');
      $('#wifi_ap').addClass('btn-danger');
    }
});
$('#wifi_conf').click(function(){
  setWiFi();
});

// Validadores
$('#wg_pulse').keyup(function() {
    var $th = $(this);
    $th.val( $th.val().replace(/[^0-9]/g, function(str) { return ''; } ) );
    if($th.val() > 200)
    {
      $th.val( $th.val().slice(0, $th.val().length-1) );
    }
});
$('#wg_pulse').blur(function() {
    var $th = $(this);
    if($th.val() < 40)
    {
      $th.val(40);
    }
});
$('#wg_interval').keyup(function() {
    var $th = $(this);
    $th.val( $th.val().replace(/[^0-9]/g, function(str) { return ''; } ) );
    if($th.val() > 2000)
    {
      $th.val( $th.val().slice(0, $th.val().length-1) );
    }
});
$('#wg_interval').blur(function() {
    var $th = $(this);
    if($th.val() < 200)
    {
      $th.val(200);
    }
});
$('#wg_size').keyup(function() {
    var $th = $(this);
    $th.val( $th.val().replace(/[^0-9]/g, function(str) { return ''; } ) );
});
$('#wg_size').blur(function() {
    var $th = $(this);
    if($th.val() > 37)
    {
      $th.val('37');
    }else if ($th.val() < 26){
      $th.val('26');
    }
});
$('#wg_odd_pos').keyup(function() {
    var $th = $(this);
    $th.val( $th.val().replace(/[^0-9]/g, function(str) { return ''; } ) );
});
$('#wg_odd_pos').blur(function() {
    var $th = $(this);
    if($th.val() >= $('#wg_size').val())
    {
      $th.val($('#wg_size').val() - 1);
    }else if ($th.val() < 0){
      $th.val('0');
    }
});
$('#wg_odd_mask_high').keyup(function() {
    var $th = $(this);
    if($('#wg_size').val() <= 32)
    {
      $th.val('0');
    }else{
      var $maxSize = $('#wg_size').val() - 32;
      console.log("max size: " + $maxSize);
      $th.val( $th.val().replace(/[^0-1]/g, function(str) { return ''; } ) );
      if($th.val().length > $maxSize)
      {
        $th.val($th.val().slice($th.val().length - $maxSize, $th.val().length));
      }
    }
});
$('#wg_odd_mask_low').keyup(function() {
    var $th = $(this);
    var $maxSize;
    $th.val( $th.val().replace(/[^0-1]/g, function(str) { return ''; } ) );
    if ($('#wg_size').val() <= 32) {
      $maxSize = $('#wg_size').val();
    }else{
      $maxSize = 32;
    }
    if($th.val().length > $maxSize)
    {
      $th.val($th.val().slice($th.val().length - $maxSize, $th.val().length));
    }
});
$('#wg_even_pos').keyup(function() {
    var $th = $(this);
    $th.val( $th.val().replace(/[^0-9]/g, function(str) { return ''; } ) );
});
$('#wg_even_pos').blur(function() {
    var $th = $(this);
    if($th.val() >= $('#wg_size').val())
    {
      $th.val($('#wg_size').val() - 1);
    }else if ($th.val() < 0){
      $th.val('0');
    }
});
$('#wg_even_mask_high').keyup(function() {
    var $th = $(this);
    if($('#wg_size').val() <= 32)
    {
      $th.val('0');
    }else{
      var $maxSize = $('#wg_size').val() - 32;
      $th.val( $th.val().replace(/[^0-1]/g, function(str) { return ''; } ) );
      if($th.val().length > $maxSize)
      {
        $th.val($th.val().slice($th.val().length - $maxSize, $th.val().length));
      }
    }
});
$('#wg_even_mask_low').keyup(function() {
    var $th = $(this);
    var $maxSize;
    $th.val( $th.val().replace(/[^0-1]/g, function(str) { return ''; } ) );
    if ($('#wg_size').val() <= 32) {
      $maxSize = $('#wg_size').val();
    }else{
      $maxSize = 32;
    }
    if($th.val().length > $maxSize)
    {
      $th.val($th.val().slice($th.val().length - $maxSize, $th.val().length));
    }
});


// Dropdown functions
jQuery(function($){
  $('body').on('click', '.change-baudRate-item', function() {
    var baudRate_speed = $(this).attr('rel');
    console.log("Modificando BaudRate " + baudRate_speed);
    $('#serial_baudRate').html("BaudRate: " + baudRate_speed);
  });
});
   
$('a[data-toggle=\"tab\"]').on('shown.bs.tab', function (e) {   
  tab_pane = $(e.target).attr("href")  
  console.log('activated ' + tab_pane );  

  // IE10, Firefox, Chrome, etc.
  if (history.pushState) 
    window.history.pushState(null, null, tab_pane);
  else 
    window.location.hash = tab_pane;
  
  if(tab_pane=='#tab_wiegand')
  {
    console.log("Tab " + tab_pane + " loaded!");
    styleId(".col-de-com");
    styleId(".col-iz-com");
    styleId(".col-de-form");
    styleId(".col-iz-form");
    getWgCom();
    getWgFormat();
  }
  if(tab_pane=='#tab_serial')
  {
    console.log("Tab " + tab_pane + " loaded!");
    getSerial();
  }
  if(tab_pane=='#tab_wifi')
  {
    console.log("Tab " + tab_pane + " loaded!");
    getWiFi();
  }
});

function styleId(style){
  var ancho = 0;
    $(style).each(function(ix) {
      var sp = $(this);
      if (ancho <= sp.width()) 
        ancho = sp.width();
    });
    $(style).width(ancho);
}

function setWiegandSystem(){
  var _wd0 = get_Value($('#wg_wd0').text());
  var _wd1 = get_Value($('#wg_wd1').text());
  var _deb = get_Value($('#wg_debug').text());
  if(_deb == "Enabled"){_deb = "1";}else{_deb = "0";}
  console.log("Data 0: " + _wd0 + ", Data 1: " + _wd1 + ", Debug:" + _deb);
  $.post("setWiegandSystem", {wd0:_wd0, wd1:_wd1, debug:_deb}).done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
  },'JSON').fail(function(err){
    console.log("Error Wiegand System update " + JSON.stringify(err));
  });
} 

function setWgCom(){
  console.log("Setting Wiegand Communication");
  var _pulse = $('#wg_pulse').val();
  var _interval = $('#wg_interval').val();
  $.post("/setWiegandCommunication", {pulse:_pulse, interval:_interval}).done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
  },'JSON').fail(function(err){
    console.log("Error Wiegand System update " + JSON.stringify(err));
  });
}

function setWgFormat(){
  var _size = $('#wg_size').val();
  var _odd_pos = $('#wg_odd_pos').val();
  var _odd_mask_high = $('#wg_odd_mask_high').val();
  var _odd_mask_low = $('#wg_odd_mask_low').val();
  var _even_pos = $('#wg_even_pos').val();
  var _even_mask_high = $('#wg_even_mask_high').val();
  var _even_mask_low = $('#wg_even_mask_low').val();
  _odd_mask_high = parseInt(_odd_mask_high, 2);
  _odd_mask_low = parseInt(_odd_mask_low, 2);
  _even_mask_high = parseInt(_even_mask_high, 2);
  _even_mask_low = parseInt(_even_mask_low, 2);
  console.log("Requesting Wiegand Format");
  $.post("/setWiegandFormat", {size:_size, oddBit:_odd_pos, oddMaskHigh:_odd_mask_high, oddMaskLow:_odd_mask_low, evenBit:_even_pos, evenMaskHigh:_even_mask_high, evenMaskLow:_even_mask_low}).done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
  },'JSON').fail(function(err){
    console.log("Error Wiegand System update " + JSON.stringify(err));
  });
}

function setWiegandWebClient(){
  var _mt = get_Value($('#wg_method').text());
  var _ipAddress = $('#wg-client-ip').val();
  var _port = $('#wg-client-port').text();
  var _url = $('#wg-client-url').text();
  var _id = $('#wg-client-id').text();
  var _enabled = "1";

  $.post("/setWiegandWebClient", {method:_mt, ip:_ipAddress, port:_port, url:_url, id:_id, enabled:_enabled}).done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
    },'JSON').fail(function(err){
    console.log("Error Wiegand System update " + JSON.stringify(err));
  });
}

function setSerial(){
  var _baudRate = get_Value($('#serial_baudRate').html()); //.replace(/BaudRate: /g, '');
  console.log(_baudRate);
  $.post("/setSerial", {baudRate:_baudRate}).done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
    },'JSON').fail(function(err){
    console.log("Error Serial BaudRate update " + JSON.stringify(err));
  });
}

function setWiFi(){
  alert ("Se reiniciara la placa electronica para aplicar los cambios!");
  console.log("Setting WiFi configuration");
  var _ssid = $('#ssid').val();
  var _pass = $('#pass').val();
  var _ssid_ap = $('#ssid_ap').val();
  var _pass_ap = $('#pass_ap').val();
  var _sta_enabled;
  if($('#wifi_sta').html() == "On"){_sta_enabled = 1;}else{_sta_enabled = 0;};
  var _ap_enabled;
  if($('#wifi_ap').html() == "On"){_ap_enabled = 1;}else{_ap_enabled = 0;};

  $.post("/setWiFi", {SSID:_ssid, Pass:_pass, apSSID:_ssid_ap, apPass:_pass_ap, ap:_ap_enabled, client:_sta_enabled}).done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
  },'JSON').fail(function(err){
    console.log("Error Wiegand System update " + JSON.stringify(err));
  });
}

function getWgCom(){
  console.log("Requesting Wiegand Communication");
  $.get("/getWgCom.json").done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
    $('#wg_pulse').val(data.pulse);
    $('#wg_interval').val(data.interval);
  },'JSON').fail(function(err){
    console.log("Error Wiegand System update " + JSON.stringify(err));
  });
}

function getWgFormat(){
  console.log("Requesting Wiegand Format");
  $.get("/getWgFormat.json").done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
    $('#wg_size').val(data.size);
    $('#wg_odd_pos').val(data.oddBit);
    $('#wg_odd_mask_high').val((data.oddMaskHigh >>> 0).toString(2));
    $('#wg_odd_mask_low').val((data.oddMaskLow >>> 0).toString(2));
    $('#wg_even_pos').val(data.evenBit);
    $('#wg_even_mask_high').val((data.evenMaskHigh >>> 0).toString(2));
    $('#wg_even_mask_low').val((data.evenMaskLow >>> 0).toString(2));
  },'JSON').fail(function(err){
    console.log("Error Wiegand System update " + JSON.stringify(err));
  });
}

function getSerial(){
  console.log("Requesting Serial BaudRate");
  $.get("/getSerial.json").done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
    $('#serial_baudRate').html("BaudRate: " + data.baudRate);
  },'JSON').fail(function(err){
    console.log("Error Serial BaudRate update " + JSON.stringify(err));
  });
}

function getWiFi(){
  console.log("Requesting WiFi configuration");
  $.get("/getWiFi.json").done(function(data){
    console.log("request return: " + JSON.stringify(data)); 
    $('#ssid').val(data.ssid);
    $('#pass').val(data.password);
    $('#ssid_ap').val(data.ssid_ap);
    $('#pass_ap').val(data.password_ap);
	console.log("Client state: " + data.client);
    if(data.client == true)
    {
      $('#wifi_sta').html("On");
      $('#wifi_sta').removeClass('btn-danger');
      $('#wifi_sta').addClass('btn-success');
    }else{
      $('#wifi_sta').html("Off");
      $('#wifi_sta').removeClass('btn-success');
      $('#wifi_sta').addClass('btn-danger');
    }
	console.log("Ap state: " + data.ap);
    if(data.ap == true)
    {
      $('#wifi_ap').html("On");
      $('#wifi_ap').removeClass('btn-danger');
      $('#wifi_ap').addClass('btn-success');
    }else{
      $('#wifi_ap').html("Off");
      $('#wifi_ap').removeClass('btn-success');
      $('#wifi_ap').addClass('btn-danger');
    }
  },'JSON').fail(function(err){
    console.log("Error Wiegand System update " + JSON.stringify(err));
  });
}

function setPower(state){
  if(state == '0' || state == '1'){
    $.get("power.php?action=" + state);
  }
} 

function get_Value(dataValue){
  return dataValue.slice(dataValue.search(": ")+2);
}

