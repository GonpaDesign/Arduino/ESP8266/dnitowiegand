#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "FS.h"
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "AsyncJson.h"
#include <SoftwareSerial.h>
#include "global.h"
#include "helpers.h"

#define DEBUG 1
#define Firmware "Firmware v1.2.1"

#define swTX 15
#define swRX 13
#define WD0 4
#define WD1 5

SoftwareSerial sw(swRX, swTX, false, 256);
AsyncWebServer server(80);

using namespace std;

dni DNI;
serialConf serial;
wgComm wg;
wgFormat wgFm;
wifiConf wifi;

void setup() {
  WiFi.mode(WIFI_AP_STA);

  pinMode(WD0, OUTPUT);
  pinMode(WD1, OUTPUT);
  pinMode(D4, INPUT);

  digitalWrite(WD0, HIGH);
  digitalWrite(WD1, HIGH);

  Serial.begin(115200);
  while (!Serial) {}
  Serial.println();
  Serial.println("JPH S.A.");
  Serial.println("Conversor Serie-Wiegand");
  Serial.println(Firmware);
  Serial.println("Product Id: SW02");
  Serial.print("Product UID: "); Serial.println(system_get_chip_id());
  Serial.println();

  Serial.println("Inicializando sistema de archivos");
  Serial.println();
  fsBegin();
  Serial.println("Inicializando servidor");
  setServer();

  sw.begin(serial.baudRate);
}

void loop() {
  if (sw.available()) {
    String incomming = sw.readStringUntil('\n');
    sendDNI(incomming);
  }
}

bool isNew(String dniData) {
  byte count = 0;
  for (int index = 0; index <= dniData.length(); index++) {
    if (dniData[index] == '@') {
      count++;
    }
  }

  if (count >= 10) {
    return false;
  }
  return true;
}

void sendDNI(String dniInfo) {
  Serial.print("El DNI escaneado es: "); Serial.println(isNew(dniInfo) ? "Nuevo" : "Viejo");
  if (isNew(dniInfo)) {
    parseNew(dniInfo);
  }
  else {
    parseOld(dniInfo);
  }

  sendWiegand(toWiegand());

  if ((wifi.client == true) && (WiFi.status() == WL_CONNECTED)) {
    HTTPClient http;

    http.begin("http://192.168.0.60:80/dni.php");
    http.addHeader("Content-Type", "text/plain");

    int httpResponseCode = http.POST("dni=" + DNI.dni);   //Send the actual POST request

    if (httpResponseCode > 0) {
      Serial.println(httpResponseCode);
    }
    else {
      Serial.println("Error on sending POST");
    }

    http.end();  //Free resources
  }
}

void parseOld(String data) {
  DNI.tramite = getIntValue(data, 10, '@');
  DNI.apellido = getStringValue(data, 4, '@');
  DNI.nombre = getStringValue(data, 5, '@');
  DNI.sexo = getStringValue(data, 8, '@');
  DNI.ejemplar = getStringValue(data, 2, '@');
  DNI.nacimiento = getStringValue(data, 7, '@');
  DNI.emision = getStringValue(data, 9, '@');

  String dniTemp = getStringValue(data, 1, '@');
  dniTemp.replace("@", "");
  dniTemp.replace("F", "");
  dniTemp.replace("M", "");

  DNI.dni = dniTemp.toInt();

  if (DEBUG) {
    Serial.print("Tramite N°: "); Serial.println(DNI.tramite);
    Serial.print("Apellido: "); Serial.println(DNI.apellido);
    Serial.print("Nombre: "); Serial.println(DNI.nombre);
    Serial.print("Sexo: "); Serial.println(DNI.sexo);
    Serial.print("DNI: "); Serial.println(DNI.dni);
    Serial.print("Ejemplar: "); Serial.println(DNI.ejemplar);
    Serial.print("Fecha de Nacimiento: "); Serial.println(DNI.nacimiento);
    Serial.print("Fecha de Emision: "); Serial.println(DNI.emision);
  }
}

void parseNew(String data) {
  DNI.tramite = getIntValue(data, 1, '@');
  DNI.apellido = getStringValue(data, 2, '@');
  DNI.nombre = getStringValue(data, 3, '@');
  DNI.sexo = getStringValue(data, 4, '@');
  DNI.ejemplar = getStringValue(data, 6, '@');
  DNI.nacimiento = getStringValue(data, 7, '@');
  DNI.emision = getStringValue(data, 8, '@');

  String dniTemp = getStringValue(data, 5, '@');
  dniTemp.replace("F", "");
  dniTemp.replace("M", "");
  DNI.dni = dniTemp.toInt();

  if (DEBUG) {
    Serial.print("Tramite N°: "); Serial.println(DNI.tramite);
    Serial.print("Apellido: "); Serial.println(DNI.apellido);
    Serial.print("Nombre: "); Serial.println(DNI.nombre);
    Serial.print("Sexo: "); Serial.println(DNI.sexo);
    Serial.print("DNI: "); Serial.println(DNI.dni);
    Serial.print("Ejemplar: "); Serial.println(DNI.ejemplar);
    Serial.print("Fecha de Nacimiento: "); Serial.println(DNI.nacimiento);
    Serial.print("Fecha de Emision: "); Serial.println(DNI.emision);
  }
}

void fsBegin() {
  if (!SPIFFS.begin()) {
    if (DEBUG) {
      Serial.println("Status: Failed");
      Serial.println();
      Serial.println("  ---- LOADING DEFAULT CONFIGURATION ----");
      Serial.println();
    }
    return;
  }
  else {
    if (!loadWiFiConfig()) {
      Serial.println("No se puede cargar la configuración de WiFi");
    } else {
      Serial.println("Configuracion WiFi cargada");
    }
    if (!loadWiegandSys()) {
      Serial.println("No se puede cargar la configuración WiegandSys");
    } else {
      Serial.println("Configuracion WiegandSys cargada");
    }
    if (!loadWiegandConfig()) {
      Serial.println("No se puede cargar la configuración WiegandCom");
    } else {
      Serial.println("Configuracion WiegandCom cargada");
    }
    if (!loadSerialConfig()) {
      Serial.println("No se puede cargar la configuración Serial");
    } else {
      Serial.println("Configuracion Serial cargada");
    }
  }
}

bool loadWiFiConfig() {

  File configFile;
  configFile = SPIFFS.open("/wifi.json", "r");

  if (!configFile) {
    if (DEBUG == 1) {
      Serial.println("Error al abrir el archivo de configuracion wifi.json");
    }
    return false;
  }

  size_t size = configFile.size();
  if (size > 1024) {
    if (DEBUG == 1) {
      Serial.println("El tamaño del archivo de configuración WiFi es demasiado grande");
    }
    return false;
  }


  std::unique_ptr<char[]> buf(new char[size]);
  configFile.readBytes(buf.get(), size);
  StaticJsonBuffer<512> jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(buf.get());

  if (!json.success()) {
    if (DEBUG == 1) {
      Serial.println("Error al parsear archivo WiFi");
    }
    return false;
  }

  wifi.apSSID = json["apSSID"].as<String>();
  wifi.apPass = json["apPass"].as<String>();
  wifi.SSID = json["SSID"].as<String>();
  wifi.Pass = json["Pass"].as<String>();
  wifi.hidden = json.get<bool>("hidden");
  wifi.client = json.get<bool>("client");
  wifi.ap = json.get<bool>("ap");

  WiFi.disconnect();
  WiFi.softAPdisconnect();

  if (wifi.client == 1) { //Intenta modo cliente por 10seg y luego Modo AP
    Serial.println("Intentando conectarse a red wifi");
    if ((WifiMode(1) == false) and (wifi.ap == 1)) {
      WifiMode(2); //Modo AP
    }
  }
  else {
    if (wifi.ap == 1) {
      WifiMode(2); //Modo AP
      Serial.println("entra en el bucle donde el cliente es cero y ap 1");
    }
  }

  if (DEBUG) {
    json.prettyPrintTo(Serial);
    Serial.println();
  }

  return true;
}

bool loadWiegandConfig() {
  File configFile;
  configFile = SPIFFS.open("/wiegandCom.json", "r");

  if (!configFile) {
    if (DEBUG == 1) {
      Serial.println("Error al abrir el archivo de configuracion wiegandCom.json");
    }
    return false;
  }

  size_t size = configFile.size();
  if (size > 1024) {
    if (DEBUG == 1) {
      Serial.println("El tamaño del archivo de configuración wiegandCom es demasiado grande");
    }
    return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  configFile.readBytes(buf.get(), size);
  StaticJsonBuffer<712> jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(buf.get());

  if (!json.success()) {
    if (DEBUG == 1) {
      Serial.println("Error al parsear archivo wiegandCom");
    }
    return false;
  }

  wgFm.size = json["Format"]["size"].as<int>();
  wgFm.oddBit = json["Format"]["oddBit"].as<int>();
  wgFm.oddMaskLow = json["Format"]["oddMaskLow"].as<unsigned long>();
  wgFm.oddMaskHigh = json["Format"]["oddMaskHigh"].as<unsigned long>();
  wgFm.evenBit = json["Format"]["evenBit"].as<int>();
  wgFm.evenMaskLow = json["Format"]["evenMaskLow"].as<unsigned long>();
  wgFm.evenMaskHigh = json["Format"]["evenMaskHigh"].as<unsigned long>();

  if (DEBUG) {
    json.prettyPrintTo(Serial);
    Serial.println();
  }
  return true;
}

bool loadWiegandSys() {
  File configFile;
  configFile = SPIFFS.open("/wiegandSys.json", "r");

  if (!configFile) {
    if (DEBUG == 1) {
      Serial.println("Error al abrir el archivo de configuracion wiegandSys.json");
    }
    return false;
  }

  size_t size = configFile.size();
  if (size > 1024) {
    if (DEBUG == 1) {
      Serial.println("El tamaño del archivo de configuración wiegandSys es demasiado grande");
    }
    return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  configFile.readBytes(buf.get(), size);
  StaticJsonBuffer<712> jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(buf.get());

  if (!json.success()) {
    if (DEBUG == 1) {
      Serial.println("Error al parsear archivo wiegandSys");
    }
    return false;
  }

  wg.pulse = json["System"]["pulse"].as<int>();
  wg.interval = json["System"]["interval"].as<int>();

  if (DEBUG) {
    json.prettyPrintTo(Serial);
    Serial.println();
  }
  return true;
}

bool loadSerialConfig() {
  File configFile;
  configFile = SPIFFS.open("/serial.json", "r");

  if (!configFile) {
    if (DEBUG == 1) {
      Serial.println("Error al abrir el archivo de configuracion serial.json");
    }
    return false;
  }

  size_t size = configFile.size();
  if (size > 1024) {
    if (DEBUG == 1) {
      Serial.println("El tamaño del archivo de configuración serial es demasiado grande");
    }
    return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  configFile.readBytes(buf.get(), size);

  StaticJsonBuffer<256> jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(buf.get());

  if (!json.success()) {
    if (DEBUG == 1) {
      Serial.println("Error al parsear archivo serial");
    }
    return false;
  }

  serial.baudRate = json["baudRate"];
  Serial.end();
  Serial.begin(serial.baudRate);

  if (DEBUG) {
    json.prettyPrintTo(Serial);
    Serial.println();
  }
  return true;
}

bool WifiMode(int caso) {
  float reconn = 10; //Cada reintento toma 1 seg
  switch (caso) {
    case 1: //Cliente
      WiFi.begin(wifi.SSID.c_str(), wifi.Pass.c_str());
      if ((reconn > 0) and (WiFi.status() != WL_CONNECTED)) {
        WiFi.mode(WIFI_STA); //Inicia el WiFi modo Estacion. Opciones: AP - STA - STA+AP
        //Serial.print("MAC: "); Serial.println(WiFi.macAddress());
        Serial.print("Connecting");
      }
      while ((WiFi.status() != WL_CONNECTED) and (reconn > 0) ) {
        delay(500);
        Serial.print(".");
        reconn -= 0.5;
      }
      if ((WiFi.status() == WL_CONNECTED) and reconn >= 0) {
        WiFi.softAPdisconnect(); //Elimina el punto de acceso creado anteriormente
        Serial.println("Connected");
        Serial.print("Dirección IP: "); Serial.println(WiFi.localIP());
        return true;
        reconn = -1;
      }
      else {
        Serial.println("No se pudo conectar en modo Cliente");
        return false;
      }
      break;
    case 2: //AP
      WiFi.mode(WIFI_AP);
      WiFi.softAP(wifi.apSSID.c_str(), wifi.apPass.c_str(), 1, wifi.hidden); //WiFi.softAP (ssid, password, channel, (hidden = 0 : Oculto. hidden = 1 : Visible))
      Serial.println("WiFi created");
      Serial.print("SSID: "); Serial.println(wifi.apSSID.c_str());
      Serial.print("Password: "); Serial.println(wifi.apPass.c_str());
      Serial.print("Dirección IP Soft-AP = "); Serial.println(WiFi.softAPIP());
      reconn -= 1;
      return true;
      break;
  }
}

String toWiegand() {
  Serial.print("DNI Number: "); Serial.println(DNI.dni);
  Serial.print("Binary DNI: "); Serial.println(DNI.dni, BIN);

  String temp;

  for (byte index = 0; index < 34; index++) {
    if (index < 2) {
      temp += "0";
    }
    else {
      int mask = 1;
      mask <<= (33 - (index + 1));
      mask &= DNI.dni;
      if (mask == 0) {
        temp += "0";
      }
      else {
        temp += "1";
      }
    }
  }

  Serial.println(temp);

  temp = parityCalc(temp);

  return temp;
}

String parityCalc(String original) {
  String wg = original;
  int pCount = 0;
  for (byte index = 0; index < 17; index++) {
    if (wg[index] != '0') {
      pCount++;
    }
  }

  if ((pCount % 2) != 0) {
    wg[0] = '1';
  }

  pCount = 0;
  for (byte index = 17; index < 34; index++)
  {
    if (wg[index] != '0') {
      pCount++;
    }
  }

  if ((pCount % 2) == 0)
  {
    wg[33] = '1';
  }

  Serial.println(wg);

  return wg;
}

void sendWiegand(String code) {
  for (byte index = 0; index < 34; index++) {
    code[index] == '0' ? writeD0() : writeD1();
    delayMicroseconds(wg.interval);
  }
}

void writeD0() {
  digitalWrite(WD0, LOW);
  delayMicroseconds(wg.pulse);
  digitalWrite(WD0, HIGH);
}

void writeD1() {
  digitalWrite(WD1, LOW);
  delayMicroseconds(wg.pulse);
  digitalWrite(WD1, HIGH);
}

String processor(const String& var) {
  if (var == "PLACEHOLDER") {
    return String(random(1, 20));
  }

  return String();
}

void setServer() {
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    //request->send(SPIFFS, "/home.html", "text/html", false, processor);
    request->send(SPIFFS, "/index.html", String(), false, processor);
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
  });
  server.on("/redirect/external", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->redirect("http://www.jphlions.com/");
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
  });
  server.on("/redirect/internal", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->redirect("https://www.jphlions.com/");
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
  });

  //Parameters
  server.on("/reset", HTTP_POST, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }

    AsyncResponseStream *response = request->beginResponseStream("application/json");
    DynamicJsonBuffer jsonBuffer;
    JsonObject &r = jsonBuffer.createObject();
    r["readyState"] = "1";
    r.printTo(*response);
    request->send(response);

    ESP.reset();
  });
  server.on("/setWiegandCommunication", HTTP_POST, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
    int paramsNr = request->params();
    DynamicJsonBuffer jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();

    for (int i = 0; i < paramsNr; i++) {
      AsyncWebParameter* p = request->getParam(i);
      root[p->name()] = p->value();
    }
    if (DEBUG) {
      root.prettyPrintTo(Serial);
      Serial.println();
    }

    File file = SPIFFS.open("/wiegandCom.json", "w");
    if (!file) {
      Serial.println("Failed to open file wiegandSys server");
      return;
    }
    if (root.printTo(file) == 0) {
      Serial.println("Failed to write to file wiegandSys");
    }
    file.close();

    AsyncResponseStream *response = request->beginResponseStream("application/json");
    JsonObject &r = jsonBuffer.createObject();
    r["readyState"] = "1";
    r.printTo(*response);
    request->send(response);
  });
  server.on("/setWiegandFormat", HTTP_POST, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
    int paramsNr = request->params();
    DynamicJsonBuffer jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();

    for (int i = 0; i < paramsNr; i++) {
      AsyncWebParameter* p = request->getParam(i);
      root[p->name()] = p->value();
    }
    if (DEBUG) {
      root.prettyPrintTo(Serial);
      Serial.println();
    }

    File file = SPIFFS.open("/wiegandSys.json", "w");
    if (!file) {
      Serial.println("Failed to create file wiegandSys");
      return;
    }
    if (root.printTo(file) == 0) {
      Serial.println("Failed to write to file wiegandSys");
    }
    file.close();

    AsyncResponseStream *response = request->beginResponseStream("application/json");
    JsonObject &r = jsonBuffer.createObject();
    r["readyState"] = "1";
    r.printTo(*response);
    request->send(response);
  });
  server.on("/setWiegandWebClient", HTTP_POST, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
    int paramsNr = request->params();
    DynamicJsonBuffer jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();

    for (int i = 0; i < paramsNr; i++) {
      AsyncWebParameter* p = request->getParam(i);
      root[p->name()] = p->value();
    }
    if (DEBUG) {
      root.prettyPrintTo(Serial);
      Serial.println();
    }

    AsyncResponseStream *response = request->beginResponseStream("application/json");
    JsonObject &r = jsonBuffer.createObject();
    r["readyState"] = "1";
    r.printTo(*response);
    request->send(response);
  });
  server.on("/setSerial", HTTP_POST, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
    int paramsNr = request->params();
    DynamicJsonBuffer jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();

    for (int i = 0; i < paramsNr; i++) {
      AsyncWebParameter* p = request->getParam(i);
      root[p->name()] = p->value();
    }

    serial.baudRate = root["baudRate"];
    Serial.end();
    Serial.begin(serial.baudRate);

    if (DEBUG) {
      root.prettyPrintTo(Serial);
      Serial.println();
    }

    File file = SPIFFS.open("/serial.json", "w");
    if (!file) {
      Serial.println("Failed to create file serial");
      return;
    }
    if (root.printTo(file) == 0) {
      Serial.println("Failed to write to file serial");
    }
    file.close();

    AsyncResponseStream *response = request->beginResponseStream("application/json");
    JsonObject &r = jsonBuffer.createObject();
    r["readyState"] = "1";
    r.printTo(*response);
    request->send(response);
  });
  server.on("/setWiFi", HTTP_POST, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
    int paramsNr = request->params();
    DynamicJsonBuffer jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();

    for (int i = 0; i < paramsNr; i++) {
      AsyncWebParameter* p = request->getParam(i);
      root[p->name()] = p->value();
    }

    if (DEBUG) {
      root.prettyPrintTo(Serial);
      Serial.println();
    }
    
    File file = SPIFFS.open("/wifi.json", "w");
    if (!file) {
      Serial.println("Failed to create file wifi");
      return;
    }
    if (root.printTo(file) == 0) {
      Serial.println("Failed to write to file wifi");
    }
    file.close();

    AsyncResponseStream *response = request->beginResponseStream("application/json");
    JsonObject &r = jsonBuffer.createObject();
    r["readyState"] = "1";
    r.printTo(*response);
    request->send(response);

    ESP.restart();
    
  });

  // -- Requests --
  server.on("/getWgCom.json", HTTP_GET, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
    AsyncResponseStream *response = request->beginResponseStream("application/json");
    DynamicJsonBuffer jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();
    root["pulse"] = wg.pulse;
    root["interval"] = wg.interval;
    root.printTo(*response);
    request->send(response);
  });
  server.on("/getWgFormat.json", HTTP_GET, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
    AsyncResponseStream *response = request->beginResponseStream("application/json");
    DynamicJsonBuffer jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();
    root["size"] = wgFm.size;
    root["oddBit"] = wgFm.oddBit;
    root["oddMaskLow"] = wgFm.oddMaskLow;
    root["oddMaskHigh"] = wgFm.oddMaskHigh;
    root["evenBit"] = wgFm.evenBit;
    root["evenMaskLow"] = wgFm.evenMaskLow;
    root["evenMaskHigh"] = wgFm.oddMaskHigh;
    root.printTo(*response);
    request->send(response);
  });
  server.on("/getSerial.json", HTTP_GET, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
    AsyncResponseStream *response = request->beginResponseStream("application/json");
    DynamicJsonBuffer jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();
    root["baudRate"] = serial.baudRate;
    root.printTo(*response);
    request->send(response);
  });
  server.on("/getWiFi.json", HTTP_GET, [](AsyncWebServerRequest * request) {
    if (DEBUG) {
      Serial.print("Received request from: ");
      Serial.println(request->client()->remoteIP());
    }
    AsyncResponseStream *response = request->beginResponseStream("application/json");
    DynamicJsonBuffer jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();
    root["ssid_ap"] = wifi.apSSID;
    root["password_ap"] = wifi.apPass;
    root["ssid"] = wifi.SSID;
    root["password"] = wifi.Pass;
    root["ap"] = wifi.ap;
    root["client"] = wifi.client;
    root.printTo(*response);
    request->send(response);
  });

  // -- Imagenes ---
  server.on("/img/logo.png", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/img/logo.png", "text/img");
  });

  // -- CSS ---
  server.on("/css/bootstrap-table.min.css", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/css/bootstrap-table.min.css", "text/css");
  });
  server.on("/css/bootstrap.min.css", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/css/bootstrap.min.css", "text/css");
  });
  server.on("/css/bootstrap.css", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/css/bootstrap.css", "text/css");
  });

  // -- JS ---
  server.on("/js/script.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/script.js", "text/javascript");
  });
  server.on("/js/bootstrap.min.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/bootstrap.min.js", "text/javascript");
  });
  server.on("/js/bootstrap-table.min.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/bootstrap-table.min.js", "text/javascript");
  });
  server.on("/js/jquery.min.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/jquery.min.js", "text/javascript");
  });
  server.on("/js/loader.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/loader.js", "text/javascript");
  });
  server.onNotFound([](AsyncWebServerRequest * request) {
    request->send(404, "text/plain", "The content you are looking for was not found.");
  });
  server.begin();
}
